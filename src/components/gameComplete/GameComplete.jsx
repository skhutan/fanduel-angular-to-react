import React from 'react'

const GameComplete = ({ playerScore, played, restart }) => (
  <>
    <h1>Game Finished</h1>
    <div>
      You scored: {playerScore} out of {played} 🎊
    </div>
    <button onClick={restart}>RESTART</button>
  </>
)

export default GameComplete
