import { connect } from 'react-redux'
import { startGame } from '../../store/actions'
import GameComplete from './gameComplete'

const mapStateToProps = ({ game }) => game
const mapDispatchToProps = { restart: startGame }

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(GameComplete)
