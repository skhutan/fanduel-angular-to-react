import React from 'react'

import Welcome from '../welcome'
import Guessing from '../guessing'
import GameComplete from '../gameComplete'

const Game = ({ step }) => (
  <>
    {step === 'WELCOME' && <Welcome />}
    {step === 'GUESSING' && <Guessing />}
    {step === 'GAME_COMPLETE' && <GameComplete />}
  </>
)

export default Game
