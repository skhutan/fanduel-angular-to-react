import { connect } from 'react-redux'
import Game from './game'

const mapStateToProps = ({ game }) => ({ step: game.step })

export default connect(mapStateToProps)(Game)
