import { connect } from 'react-redux'
import Guessing from './guessing'
import { guessPlayer, playersLoaded } from '../../store/actions'

const mapStateToProps = ({ game }) => {
  if (!game.players) {
    return {}
  }

  return {
    ...game,
    playerA: game.players.find(({ id }) => game.nextPair[0] === id),
    playerB: game.players.find(({ id }) => game.nextPair[1] === id),
  }
}

const mapDispatchToProps = { guessPlayer, playersLoaded }

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Guessing)
