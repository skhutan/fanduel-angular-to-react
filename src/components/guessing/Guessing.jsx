import React, { useState, useEffect } from 'react'

import PlayerOption from '../playerOption'

const Guessing = ({ playerA, playerB, playersLoaded, guessPlayer }) => {
  const [isLoading, setisLoading] = useState(false)
  const [hasGuessed, setHasGuessed] = useState(false)
  const [lastGuessedPlayerId, setLastGuessedPlayerId] = useState('')

  const next = () => {
    guessPlayer(lastGuessedPlayerId)
    setHasGuessed(true)
  }

  const guess = player => {
    setLastGuessedPlayerId(player.id)
    setHasGuessed(true)
  }

  useEffect(() => {
    const fetchPlayers = async () => {
      setisLoading(false)
      await playersLoaded()
      setisLoading(true)
    }
    fetchPlayers()
  }, [])

  return (
    isLoading && (
      <>
        Which Player has the highest FPPG? (Fantasy Points per Game)
        <PlayerOption player={playerA} hasGuessed={hasGuessed} guess={guess} />
        <PlayerOption player={playerB} hasGuessed={hasGuessed} guess={guess} />
        {hasGuessed && <button onClick={next}>NEXT PLAYERS</button>}
      </>
    )
  )
}

export default Guessing
