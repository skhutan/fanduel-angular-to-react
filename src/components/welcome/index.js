import { connect } from 'react-redux'
import { startGame } from '../../store/actions'
import Welcome from './Welcome'

const mapDispatchToProps = { startGame }

export default connect(
  null,
  mapDispatchToProps
)(Welcome)
