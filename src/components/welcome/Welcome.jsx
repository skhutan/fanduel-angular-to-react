import React from 'react'

const Welcome = ({ startGame }) => (
  <>
    <h1>Welcome</h1>
    <p>
      <button onClick={startGame}>Start</button>
    </p>
  </>
)

export default Welcome
