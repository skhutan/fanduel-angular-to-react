import React from 'react'

const PlayerOption = ({ player, hasGuessed, guess }) => {
  const altText = `image for ${player.first_name} ${player.last_name}`

  return (
    <div className="flex-center" onClick={() => guess(player)}>
      <img height="100px" width="100px" src={player.images.default.url} alt={altText} />
      {player.first_name} {player.last_name}
      <br />
      {hasGuessed ? player.fppg : null}
    </div>
  )
}

export default PlayerOption
