import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'

import configureStore from './store'
import Game from './components/game'

import './index.css'

const mountElement = document.getElementById('root')

const App = Game

render(
  <Provider store={configureStore()}>
    <App />
  </Provider>,
  mountElement
)

export default App
