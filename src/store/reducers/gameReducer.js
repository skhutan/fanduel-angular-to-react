import { getNextPair } from '../../utils/getNextPair'
import { START_GAME, PLAYERS_LOADED, GUESS_PLAYER } from '../constants'

const initialState = {
  step: 'WELCOME',
}

function didGuessCorrectly(players, guessedPlayerID, currentPairIDs) {
  const guessedPlayerFPPG = players.find(({ id }) => guessedPlayerID === id).fppg
  const otherPlayerID = currentPairIDs.find(id => id !== guessedPlayerID)
  const otherPlayerFPPG = players.find(({ id }) => otherPlayerID === id).fppg
  return guessedPlayerFPPG > otherPlayerFPPG
}

function gameOver(allPlayers, seenPlayers) {
  return allPlayers.length - seenPlayers.length < 2
}

export default function gameReducer(state = initialState, action) {
  switch (action.type) {
    case START_GAME:
      return {
        ...state,
        step: 'GUESSING',
        playerScore: 0,
        played: 0,
        seenPlayerIDs: [],
      }
    case PLAYERS_LOADED:
      const nextPair = getNextPair(action.players, [])
      return {
        ...state,
        players: [...action.players],
        seenPlayerIDs: [],
        nextPair,
        playerScore: 0,
        played: 0,
      }

    case GUESS_PLAYER:
      const guessedCorrectly = didGuessCorrectly(state.players, action.id, state.nextPair)
      const newSeenPlayerIDs = [...state.seenPlayerIDs, ...state.nextPair]
      const newScore = state.playerScore + Number(guessedCorrectly)
      const played = state.played + 1
      if (gameOver(state.players, newSeenPlayerIDs)) {
        return {
          ...state,
          playerScore: newScore,
          played,
          step: 'GAME_COMPLETE',
        }
      }
      const newNextPair = getNextPair(state.players, newSeenPlayerIDs)
      return {
        ...state,
        played,
        playerScore: newScore,
        seenPlayerIDs: newSeenPlayerIDs,
        nextPair: newNextPair,
      }
    default:
      return state
  }
}
