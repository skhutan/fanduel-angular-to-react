import { START_GAME, PLAYERS_LOADED, GUESS_PLAYER } from '../constants'
import fetchPlayers from '../../utils/fetchPlayers'

export const startGame = () => dispatch => {
  dispatch({
    type: START_GAME,
  })
}

export const playersLoaded = () => dispatch => {
  return fetchPlayers().then(({ players }) => {
    console.log(players)
    dispatch({ type: PLAYERS_LOADED, players })
  })
}

export const guessPlayer = playerId => dispatch => {
  dispatch({
    type: GUESS_PLAYER,
    id: playerId,
  })
}
