const PLAYERS_JSON_URL =
  'https://gist.githubusercontent.com/liamjdouglas/79c5dc9f1ec682afac6ec0b0278175d5/raw/67ce51c2b06e44f365367c266bb00698c3f3bac4/mini_guessing_players.json'

const fetchPlayers = async () => fetch(PLAYERS_JSON_URL).then(res => res.json())

export default fetchPlayers
