# Instructions

This is an app that loads players from a JSON file and uses them to create a "guess who" game.

1. Read through and understand the code from this sandbox app.
2. Using the tool of your choosing, rewrite this app using React. This can be another sandbox of your choice or using create-react-app.
3. When you're done, please submit either the URL of the sandbox you have created or a zip file of the code of your app.

Good luck!

## Running

`npm start` will run this web app on localhost:8080
